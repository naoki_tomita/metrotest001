﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;
//using System.Runtime.Serialization.Json;
using MetroTest001;
using System.Collections.Generic;

using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Diagnostics;

namespace UnitTestProject2
{
    [TestClass]
    public class DataLoadTest
    {
        [TestMethod]
        public void StationTest()
        {
            var station = TestLoad<Station>(@"assets/data/Station.json").First();

            Assert.AreEqual("2014-09-03T11:39:51+09:00", station.Date);
            Assert.AreEqual("odpt.Railway:TokyoMetro.Hibiya", station.Railway);
            Assert.AreEqual("上野", station.Title);
            Assert.AreEqual(0.542, station.Long);
            Assert.AreEqual(156.343, station.Lat);
            Assert.AreEqual("odpt.Railway:JR-East.Utsunomiya", station.ConnectiongRailway[1]);
            Assert.AreEqual("odpt.StationFacility:TokyoMetro.Ueno", station.Facility[0]);



            //TestLoad<StationTimetable>(@"assets/data/StationTimetable.json");

            //TestLoad<TrainInformation>(@"assets/data/TrainInformation.json");
            //TestLoad<PassengerSurvey>(@"assets/data/PassengerSurvey.json");
        }

        [TestMethod]
        public void StationTimeTableTest()
        {
            var table = TestLoad<StationTimetable>(@"assets/data/StationTimetable.json").First();

            Assert.AreEqual("2014-09-16T20:42:33+09:00", table.Date);
            Assert.AreEqual("odpt.Station:TokyoMetro.Tozai.Nakano", table.Saturdays.Last().DestinationStation);
            Assert.AreEqual(true, table.Saturdays.Last().IsLast);
            Assert.AreEqual("00:11", table.Saturdays.Last().DepartureTime);
            Assert.AreEqual(false, table.Weekdays.First().IsLast);
            Assert.AreEqual("05:09", table.Weekdays.First().DepartureTime);
            Assert.AreEqual("odpt.Railway:TokyoMetro.Tozai", table.Railway);
            Assert.AreEqual(207, table.Holidays.Count);
            Assert.AreEqual(null, table.Holidays[10].Note);


            //TestLoad<StationTimetable>(@"assets/data/StationTimetable.json");

            //TestLoad<TrainInformation>(@"assets/data/TrainInformation.json");
            //TestLoad<PassengerSurvey>(@"assets/data/PassengerSurvey.json");
        }

        [TestMethod]
        public void PassengerSurveyTest()
        {
            var result = TestLoad<PassengerSurvey>(@"assets/data/PassengerSurvey.json").First();

            Assert.AreEqual("odpt.Operator:TokyoMetro", result.Operator);
            Assert.AreEqual(2013, result.SurveyYear);
            Assert.AreEqual(211539, result.PassengerJourneys);

            //TestLoad<StationTimetable>(@"assets/data/StationTimetable.json");

            //TestLoad<TrainInformation>(@"assets/data/TrainInformation.json");
            //TestLoad<PassengerSurvey>(@"assets/data/PassengerSurvey.json");
        }


        static IEnumerable<T> TestLoad<T>(string filePath) where T : IJsonLoadable, new()
        {

            JToken data;

            using (System.IO.StreamReader reader = System.IO.File.OpenText(filePath))
            {
                data = JsonLD.Util.JSONUtils.FromReader(reader);
            }

            return TestLoad<T>(data);
        }

        static IEnumerable<T> TestLoad<T>(JToken data) where T : IJsonLoadable, new()
        {

            foreach (var val in data)
            {
                var restp = new T();
                restp.Load(val);

                yield return restp;
                //restp.Disp(str => Console.WriteLine(str));
            }
            //Console.WriteLine("end");
            //Console.ReadLine();
        }
    }
}
