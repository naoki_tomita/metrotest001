﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json.Linq;

namespace MetroTest001
{

    //https://api.tokyometroapp.jp/api/v2/datapoints?rdf:type=odpt:StationTimetable&odpt:station=odpt.Station:TokyoMetro.Tozai.Otemachi&acl:consumerKey=

    /*
    class SecuritySettings // 別ファイルに切り離しておく
    {
        public static string AccessToken { get { return ACL_CONSUMERKEY; } }
    }
    */

    class Program
    {

        private static MetroData cliant;

        static void Main(string[] args)
        {
            //別クラスで定義したアクセストークンを呼び出す
            cliant = new MetroData(SecuritySettings.AccessToken);

            
            TestLoad<Station>(@"assets/data/Station.json");

            TestLoad<StationTimetable>(@"assets/data/StationTimetable.json");

            TestLoad<TrainInformation>(@"assets/data/TrainInformation.json");
            TestLoad<PassengerSurvey>(@"assets/data/PassengerSurvey.json");
            
            
            Console.ReadLine();


            TestLoad<TrainInformation>(RdfTypes.TrainInformation,
                new Dictionary<string, string> { { "odpt:railway", "odpt.Railway:TokyoMetro.Hibiya" } });
            TestLoad<PassengerSurvey>(RdfTypes.PassengerSurvey,
                new Dictionary<string, string> { { "owl:sameAs", "TokyoMetro.Ueno.2013" } });

            TestLoad<Station>(RdfTypes.Station,
                new Dictionary<string, string> {
                { "dc:title", "上野" },
                { "odpt:railway", "odpt.Railway:TokyoMetro.Hibiya" } 
                });

            TestLoad<StationTimetable>(RdfTypes.StationTimetable,
                new Dictionary<string, string> { { "odpt:station", "odpt.Station:TokyoMetro.Tozai.Otemachi" } });
            Console.ReadLine();
            return;
        }

        static void TestLoad<T>(RdfTypes rdf, IDictionary<string, string> options) where T : IJsonLoadable, new()
        {

            string url = cliant.GetRequestUrl(
                rdf, options);

            /*
             Console.WriteLine(url);
             return;
             Console.ReadLine();
            
             */
            var data = JsonLD.Util.JSONUtils.FromURL(new Uri(url));

            TestLoad<T>(data);
        }

        static void TestLoad<T>(string filePath) where T : IJsonLoadable, new()
        {

            JToken data;

            using (System.IO.StreamReader reader = System.IO.File.OpenText(filePath))
            {
                data = JsonLD.Util.JSONUtils.FromReader(reader);
            }

            TestLoad<T>(data);
        }

        static void TestLoad<T>(JToken data) where T : IJsonLoadable, new()
        {

            foreach (var val in data)
            {
                var restp = new T();
                restp.Load(val);

                restp.Disp(str => Console.WriteLine(str));
            }
            Console.WriteLine("end");
            Console.ReadLine();
        }


        static void StationTest()
        {

            string url = cliant.GetRequestUrl(RdfTypes.Station,
                new Dictionary<string, string>()
                {
                    {"dc:title", "上野" },
                    {"odpt:railway", "odpt:Railway:TokyoMetro.Hibiya" },
                });

            var data = JsonLD.Util.JSONUtils.FromURL(new Uri(url));

            /*
            var res3 = data.Value<object>("odpt:lconnectingRailwa");//[0];
            Console.WriteLine(res3.ToString());
            Console.ReadLine();
            */
            /*
            foreach (var source in data)//.Children()["odpt:connectingRailway"])
            {

                //var res7 = source.Value<Newtonsoft.Json.Linq.JArray>("odpt:connectingRailway");//.Children()["odpt:connectingRailway"];
                //Console.WriteLine(res7.Value<string>(0).ToString());
                //Console.ReadLine();
                foreach (var elm in source.Value<Newtonsoft.Json.Linq.JArray>("odpt:connectingRailway").Values<string>())
                {
                    
                    //var r5 = elm.Value<string>(0);
                    //Console.WriteLine(r5.ToString());
                    Console.WriteLine(elm.ToString());
                }
                //Console.WriteLine(val.Value<string>("").ToString());
                //
            }
            Console.ReadLine();
            */
            foreach (var val in data)
            {

                var restp = new Station();
                restp.Load(val);



                restp.Disp(str => Console.WriteLine(str));


            }
            Console.WriteLine("end");
            Console.ReadLine();
        }
        static void StationTimeTableTest()
        {

            string url = cliant.GetRequestUrl(RdfTypes.StationTimetable,
                new Dictionary<string, string>()
                {
                    {"odpt:station", "odpt.Station:TokyoMetro.Tozai.Otemachi" },
                });


            var data = JsonLD.Util.JSONUtils.FromURL(new Uri(url));

            foreach (var val in data)
            {

                var restp = new StationTimetable();
                restp.Load(val);



                restp.Disp(str => Console.WriteLine(str));

                Console.WriteLine("\nWeekdaysLast");
                restp.Weekdays.Where(p => p.IsLast).ToList()
                    .ForEach(p => p.Disp(str => Console.WriteLine("\t" + str)));

                Console.WriteLine("\nSaturedaysLast");
                restp.Saturdays.Where(p => p.IsLast).ToList()
                    .ForEach(p => p.Disp(str => Console.WriteLine("\t" + str)));

                Console.WriteLine("\nHolidaysLast");
                restp.Holidays.Where(p => p.IsLast).ToList()
                    .ForEach(p => p.Disp(str => Console.WriteLine("\t" + str)));

                restp.Weekdays.Where(p => p.CarComposition != 0).ToList()
                    .ForEach(p => p.Disp(str => Console.WriteLine("\t" + str)));
            }
            Console.WriteLine("end");
            Console.ReadLine();
        }
    }

}
