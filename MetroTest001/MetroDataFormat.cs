﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace MetroTest001
{
    public class MetroData
    {
        private const string endPoint = "https://api.tokyometroapp.jp/api/v2/";
        private readonly string accessToken;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken">開発者用アクセストークン</param>
        public MetroData(string accessToken)
        {
            this.accessToken = accessToken;
        }

        private static readonly Dictionary<RdfTypes, string> rdfTypeNameMap = new Dictionary<RdfTypes, string>()
        {
            {RdfTypes.Train, "odpt:Train"},
            {RdfTypes.TrainInformation, "odpt:TrainInformation"},
            {RdfTypes.StationTimetable, "odpt:StationTimetable"},
            {RdfTypes.StationFacility, "odpt:StationFacility"},
            {RdfTypes.PassengerSurvey, "odpt:PassengerSurvey"},
            {RdfTypes.RailwayFare, "odpt:RailwayFare"},
            {RdfTypes.Poi, "ug:Poi"},
            {RdfTypes.MlitStation, "mlit:Station"},
            {RdfTypes.MlitRailway, "mlit:Railway"},
            {RdfTypes.Station, "odpt:Station"},
            {RdfTypes.Railway, "odpt:Railway"},
        };

        public string GetRequestUrl(RdfTypes type, IDictionary<string, string> options = null)
        {
            var rdfTypeName = rdfTypeNameMap[type];
            options = options ?? new Dictionary<string, string>();
            options["rdf:type"] = rdfTypeName;
            options["acl:consumerKey"] = accessToken;

            var optionString = String.Join("&", options.Select(pair => pair.Key + "=" + pair.Value));
            var url = endPoint + "datapoints?" + optionString;
            return url;
        }
    }

    public enum RdfTypes
    {
        /// <summary>
        /// 列車ロケーション情報 	データ取得・検索API(datapoints)
        /// </summary>
        Train,
        /// <summary>
        /// 運行情報 	データ取得・検索API(datapoints)
        /// </summary>
        TrainInformation,
        /// <summary>
        /// 駅時刻表 	データ取得・検索API(datapoints)
        /// </summary>
        StationTimetable,
        /// <summary>
        /// 施設情報（駅施設情報） 	データ取得・検索API(datapoints)
        /// </summary>
        StationFacility,
        /// <summary>
        /// 駅情報（乗降人員数） 	データ取得・検索API(datapoints)
        /// </summary>
        PassengerSurvey,
        /// <summary>
        /// 運賃情報 	データ取得・検索API(datapoints)
        /// </summary>
        RailwayFare,
        /// <summary>
        /// 地物（駅出入口）情報 	データ取得・検索API(datapoints),地物情報取得・検索API(places)
        /// </summary>
        Poi,
        /// <summary>
        /// 国土交通省国土数値情報 地物（駅）情報 	地物情報取得・検索API(places)
        /// </summary>
        MlitStation,
        /// <summary>
        /// 国土交通省国土数値情報 地物（線路）情報 	地物情報取得・検索API(places)
        /// </summary>
        MlitRailway,
        /// <summary>
        /// 東京メトロ駅情報 	データ取得・検索API(datapoints),地物情報取得・検索API(places)
        /// </summary>
        Station,
        /// <summary>
        /// 東京メトロ路線情報 	データ取得・検索API(datapoints),地物情報取得・検索API(places)
        /// </summary>
        Railway,
    };

    public class StationTimetable : JsonObject
    {


        /// <summary>
        /// @context 	URL 	JSON-LD仕様に基づく @context のURL 	◯
        /// </summary>
        public string AContext { get { return _context.Value; } private set { _context.Value = value; } }
        private LabeledParameter<string> _context = new LabeledParameter<string>("@context");


        /// <summary>
        /// @id 	URN 	固有識別子(ucode) 	◯
        /// </summary>
        public string AId { get { return _id.Value; } private set { _id.Value = value; } }
        private LabeledParameter<string> _id = new LabeledParameter<string>("@id");


        /// <summary>
        /// @type 	odpt:StationTimetable 	クラス指定 	◯
        /// </summary>
        public string AType { get { return _type.Value; } private set { _type.Value = value; } }
        private LabeledParameter<string> _type = new LabeledParameter<string>("@type");


        /// <summary>
        /// owl:sameAs 	URL 	固有識別子。命名ルールは、odpt.StationTimetable:TokyoMetro.路線名.駅名.方面名 である。 	◯
        /// </summary>
        public string SameAs { get { return _sameAs.Value; } private set { _sameAs.Value = value; } }
        private LabeledParameter<string> _sameAs = new LabeledParameter<string>("owl:sameAs");

        /// <summary>
        /// dc:date 	xsd:dateTime 	データ生成時刻、e.g. 2013-01-13T15:10:00+09:00、ISO8601日付時刻形式 	◯
        /// </summary>
        public string Date { get { return _date.Value; } private set { _date.Value = value; } }
        private LabeledParameter<string> _date = new LabeledParameter<string>("dc:date");

        /// <summary>
        /// odpt:station 	odpt:Station 	駅 	◯
        /// </summary>
        public string Station { get { return _station.Value; } private set { _station.Value = value; } }
        private LabeledParameter<string> _station = new LabeledParameter<string>("odpt:station");

        /// <summary>
        /// odpt:railway 	odpt:Railway 	路線 	◯
        /// </summary>
        public string Railway { get { return _railway.Value; } private set { _railway.Value = value; } }
        private LabeledParameter<string> _railway = new LabeledParameter<string>("odpt:railway");

        /// <summary>
        /// odpt:operator 	odpt:Operator 	運行会社 	◯
        /// </summary>
        public string Operator { get { return _operator.Value; } private set { _operator.Value = value; } }
        private LabeledParameter<string> _operator = new LabeledParameter<string>("odpt:operator");

        /// <summary>
        /// odpt:railDirection 	odpt:RailDirection 	方面 	◯
        /// </summary>
        public string RailDirection { get { return _railDirection.Value; } private set { _railDirection.Value = value; } }
        private LabeledParameter<string> _railDirection = new LabeledParameter<string>("odpt:railDirection");


        /// <summary>
        /// odpt:weekdays 	Array(odpt:StationTimetableObject) 	平日（出発時間、行き先駅名等の組のリストを格納） 
        /// </summary>
        public List<StationTimetableObject> Weekdays { get { return _weekdays.Value; } }
        private LabeledList<StationTimetableObject> _weekdays
            = new LabeledList<StationTimetableObject>("odpt:weekdays");


        /// <summary>
        /// odpt:saturdays 	Array(odpt:StationTimetableObject) 	土曜日（出発時間、行き先駅名等の組のリストを格納） 	
        /// </summary>
        public List<StationTimetableObject> Saturdays { get { return _saturdays.Value; } }
        private LabeledList<StationTimetableObject> _saturdays
            = new LabeledList<StationTimetableObject>("odpt:saturdays");


        /// <summary>
        /// odpt:holidays 	Array(odpt:StationTimetableObject) 	休日（出発時間、行き先駅名等の組のリストを格納）
        /// </summary>
        public List<StationTimetableObject> Holidays { get { return _holidays.Value; } }
        private LabeledList<StationTimetableObject> _holidays
            = new LabeledList<StationTimetableObject>("odpt:holidays");

    }


    public class StationTimetableObject : JsonObject
    {
        /// <summary>
        /// odpt:departureTime 	odpt:Time 	出発時間 (ISO8601時刻形式, e.g. 05:09) 	◯
        /// </summary>
        public string DepartureTime { get { return _departureTime.Value; } private set { _departureTime.Value = value; } }
        private LabeledParameter<string> _departureTime = new LabeledParameter<string>("odpt:departureTime");

        /// <summary>
        /// odpt:destinationStation 	odpt:Station 	行き先駅 	◯
        /// </summary>
        public string DestinationStation { get { return _destinationStation.Value; } private set { _destinationStation.Value = value; } }
        private LabeledParameter<string> _destinationStation = new LabeledParameter<string>("odpt:destinationStation");

        /// <summary>
        /// odpt:trainType 	odpt:TrainType 	列車種別。各停(odpt.TrainType:Local)、急行(odpt.TrainType:Express)、快速(odpt.TrainType:Rapid)、特急(odpt.TrainType:LimitedExpress)など。 	◯
        /// </summary>
        public string TrainType { get { return _trainType.Value; } private set { _trainType.Value = value; } }
        private LabeledParameter<string> _trainType = new LabeledParameter<string>("odpt:trainType");

        /// <summary>
        /// odpt:isLast 	xsd:boolean 	最終電車の場合、true。最終電車でない場合は省略。 
        /// </summary>
        public bool IsLast { get { return _isLast.Value; } private set { _isLast.Value = value; } }
        private LabeledParameter<bool> _isLast = new LabeledParameter<bool>("odpt:isLast", false);

        /// <summary>
        /// odpt:isOrigin 	xsd:boolean 	始発駅の場合、true。始発駅ではない場合は省略。 
        /// </summary>
        public bool IsOrigin { get { return _isOrigin.Value; } private set { _isOrigin.Value = value; } }
        private LabeledParameter<bool> _isOrigin = new LabeledParameter<bool>("odpt:isOrigin", false);

        /// <summary>
        /// odpt:carComposition 	xsd:integer 	車両数（駅に停車する車両数が列車毎に異なる場合に格納される）。 	
        /// </summary>
        public int CarComposition { get { return _carComposition.Value; } private set { _carComposition.Value = value; } }
        private LabeledParameter<int> _carComposition = new LabeledParameter<int>("odpt:carComposition", -1);

        /// <summary>
        /// odpt:note 	xsd:string 	その他の注釈（接続、通過待ちなど）。
        /// </summary>
        public string Note { get { return _note.Value; } private set { _note.Value = value; } }
        private LabeledParameter<string> _note = new LabeledParameter<string>("odpt:note");

    }


    public class TrainInformation : JsonObject
    {
        /// <summary>
        /// @context 	URL 	JSON-LD仕様に基づく @context のURL 	◯
        /// </summary>
        public string AContext { get { return _context.Value; } private set { _context.Value = value; } }
        private LabeledParameter<string> _context = new LabeledParameter<string>("@context");


        /// <summary>
        /// @id 	URN 	固有識別子(ucode) 	◯
        /// </summary>
        public string AId { get { return _id.Value; } private set { _id.Value = value; } }
        private LabeledParameter<string> _id = new LabeledParameter<string>("@id");


        /// <summary>
        /// @type 	odpt:TrainInformation 	クラス指定 	◯
        /// </summary>
        public string AType { get { return _type.Value; } private set { _type.Value = value; } }
        private LabeledParameter<string> _type = new LabeledParameter<string>("@type");


        /// <summary>
        /// dc:date 	xsd:dateTime 	データ生成時刻、e.g. 2013–01–13T15:10:00+09:00、ISO8601 日付時刻形式 	◯
        /// </summary>
        public string Date { get { return _date.Value; } private set { _date.Value = value; } }
        private LabeledParameter<string> _date = new LabeledParameter<string>("dc:date");

        /// <summary>
        /// dct:valid 	xsd:dateTime 	有効期限（ISO8601 日付時刻形式） 	◯
        /// </summary>
        public string Valid { get { return _valid.Value; } private set { _valid.Value = value; } }
        private LabeledParameter<string> _valid = new LabeledParameter<string>("dct:valid");

        /// <summary>
        /// odpt:operator 	odpt:Operator 	運行会社 	◯
        /// </summary>
        public string Operator { get { return _operator.Value; } private set { _operator.Value = value; } }
        private LabeledParameter<string> _operator = new LabeledParameter<string>("odpt:operator");

        /// <summary>
        /// odpt:timeOfOrigin 	xsd:dateTime 	発生時刻（ISO8601 日付時刻形式） 	◯
        /// </summary>
        public string TimeOfOrigin { get { return _timeOfOrigin.Value; } private set { _timeOfOrigin.Value = value; } }
        private LabeledParameter<string> _timeOfOrigin = new LabeledParameter<string>("odpt:timeOfOrigin");

        /// <summary>
        /// odpt:railway 	odpt:Railway 	発生路線 	◯
        /// </summary>
        public string RailWay { get { return _railWay.Value; } private set { _railWay.Value = value; } }
        private LabeledParameter<string> _railWay = new LabeledParameter<string>("odpt:railway");

        /// <summary>
        /// odpt:trainInformationStatus 	xsd:string 	平常時は省略。
        /// </summary>
        /// <summary>
        /// 運行情報が存在する場合は「運行情報あり」を格納。遅延などの情報を取得可能な場合は、「遅延」等のテキストを格納。
        /// </summary>
        public string TrainInformationStatus
        { get { return _trainInformationStatus.Value; } private set { _trainInformationStatus.Value = value; } }
        private LabeledParameter<string> _trainInformationStatus
            = new LabeledParameter<string>("odpt:trainInformationStatus");


        /// <summary>
        /// odpt:trainInformationText 	xsd:string 	運行情報テキスト 	◯
        /// </summary>
        public string TrainInformationText
        { get { return _trainInformationText.Value; } private set { _trainInformationText.Value = value; } }
        private LabeledParameter<string> _trainInformationText
            = new LabeledParameter<string>("odpt:trainInformationText");

    }

    public class Station : JsonObject
    {

        /// <summary>
        /// @context 	URL 	JSON-LD仕様に基づく @context のURL 	◯
        /// </summary>
        public string AContext { get { return _context.Value; } private set { _context.Value = value; } }
        private LabeledParameter<string> _context = new LabeledParameter<string>("@context");

        /// <summary>
        /// @id 	URN 	固有識別子(ucode) 	◯
        /// </summary>
        public string AId { get { return _id.Value; } private set { _id.Value = value; } }
        private LabeledParameter<string> _id = new LabeledParameter<string>("@id");

        /// <summary>
        /// @type 	odpt:Station 	地物のクラス名、odpt:Station 	◯
        /// </summary>
        public string AType { get { return _type.Value; } private set { _type.Value = value; } }
        private LabeledParameter<string> _type = new LabeledParameter<string>("@type");


        /// <summary>
        /// owl:sameAs 	URL 	固有識別子。命名ルールは、odpt.Station:TokyoMetro.路線名.駅名である。 	◯
        /// </summary>
        public string SameAs { get { return _sameAs.Value; } private set { _sameAs.Value = value; } }
        private LabeledParameter<string> _sameAs = new LabeledParameter<string>("owl:sameAs");

        /// <summary>
        /// dc:title 	xsd:string 	駅名 	◯
        /// </summary>
        public string Title { get { return _title.Value; } private set { _title.Value = value; } }
        private LabeledParameter<string> _title = new LabeledParameter<string>("dc:title");

        /// <summary>
        /// dc:date 	xsd:dateTime 	駅情報の生成時刻(ISO8601 日付時刻形式)
        /// </summary>
        public string Date { get { return _date.Value; } private set { _date.Value = value; } }
        private LabeledParameter<string> _date = new LabeledParameter<string>("dc:date");

        /// <summary>
        /// geo:long 	xsd:double 	代表点の経度、10進表記 	
        /// </summary>
        public double Long { get { return _long.Value; } private set { _long.Value = value; } }
        private LabeledParameter<double> _long = new LabeledParameter<double>("geo:long");

        /// <summary>
        /// geo:lat 	xsd:double 	代表点の緯度、10進表記
        /// </summary>
        public double Lat { get { return _lat.Value; } private set { _lat.Value = value; } }
        private LabeledParameter<double> _lat = new LabeledParameter<double>("geo:lat");

        /// <summary>
        /// ug:region 	odpt:GeoDocument 	駅代表点のデータをgeojsonで取得するためのURL。取得にはアクセストークンの付与が必要。 	
        /// </summary>
        public string Region { get { return _region.Value; } private set { _region.Value = value; } }
        private LabeledParameter<string> _region = new LabeledParameter<string>("ug:region");

        /// <summary>
        /// odpt:operator 	odpt:Operator 	管理会社 	◯
        /// </summary>
        public string Operator { get { return _operator.Value; } private set { _operator.Value = value; } }
        private LabeledParameter<string> _operator = new LabeledParameter<string>("odpt:operator");


        /// <summary>
        /// odpt:railway 	odpt:Railway 	路線 	◯
        /// </summary>
        public string Railway { get { return _railway.Value; } private set { _railway.Value = value; } }
        private LabeledParameter<string> _railway = new LabeledParameter<string>("odpt:railway");


        /// <summary>
        /// odpt:connectingRailway 	Array(odpt:Railway) 	乗り換え可能路線の一覧
        /// </summary>
        public List<string> ConnectiongRailway { get { return _connectingRailway.Value; } }
        private LabeledValueList<string> _connectingRailway = new LabeledValueList<string>("odpt:connectingRailway");


        /// <summary>
        /// odpt:facility 	Array(odpt:StationFacility) 	駅施設を表すオブジェクトのURI 	◯
        /// </summary>
        public List<string> Facility { get { return _facility.Value; } }
        private LabeledValueList<string> _facility = new LabeledValueList<string>("odpt:facility");


        /// <summary>
        /// odpt:passengerSurvey 	Array(odpt:PassengerSurvey) 	駅乗降人員数を表すオブジェクトへのリンク 	◯
        /// </summary>
        public List<string> PassengerSurvey { get { return _passengerSurvey.Value; } }
        private LabeledValueList<string> _passengerSurvey = new LabeledValueList<string>("odpt:passengerSurvey");

        /// <summary>
        /// odpt:stationCode 	xsd:string 	駅コード 	◯
        /// </summary>
        public string StationCode { get { return _stationCode.Value; } private set { _stationCode.Value = value; } }
        private LabeledParameter<string> _stationCode = new LabeledParameter<string>("odpt:stationCode");

        /// <summary>
        /// odpt:exit 	Array(ug:Poi) 	駅出入口を表すオブジェクトへのリンク 	◯
        /// </summary>
        public List<string> Exit { get { return _exit.Value; } }
        private LabeledValueList<string> _exit = new LabeledValueList<string>("odpt:exit");



    }

    public class PassengerSurvey : JsonObject
    {
        /// <summary>
        /// @context 	URL 	JSON-LD仕様に基づく @context のURL 	◯
        /// </summary>
        public string AContext { get { return _context.Value; } private set { _context.Value = value; } }
        private LabeledParameter<string> _context = new LabeledParameter<string>("@context");

        /// <summary>
        /// @id 	URN 	固有識別子(ucode) 	◯
        /// </summary>
        public string AId { get { return _id.Value; } private set { _id.Value = value; } }
        private LabeledParameter<string> _id = new LabeledParameter<string>("@id");

        /// <summary>
        /// @type 	odpt:PassengerSurvey 	クラス名、odpt:PassengerSurvey 	◯
        /// </summary>
        public string AType { get { return _type.Value; } private set { _type.Value = value; } }
        private LabeledParameter<string> _type = new LabeledParameter<string>("@type");


        /// <summary>
        /// owl:sameAs 	URL 	固有識別子。 命名ルールは odpt.PassengerSurvey:TokyoMetro.駅名.調査年 である。
        /// e.g. odpt:PassengerSurvey:TokyoMetro.Tokyo.2013 	◯
        /// </summary>
        public string SameAs { get { return _sameAs.Value; } private set { _sameAs.Value = value; } }
        private LabeledParameter<string> _sameAs = new LabeledParameter<string>("owl:sameAs");

        /// <summary>
        /// odpt:operator 	odpt:Operator 	運行会社 	◯
        /// </summary>
        public string Operator { get { return _operator.Value; } private set { _operator.Value = value; } }
        private LabeledParameter<string> _operator = new LabeledParameter<string>("odpt:operator");


        /// <summary>
        /// odpt:surveyYear 	xsd:integer 	調査年度 	◯
        /// </summary>
        public int SurveyYear { get { return _surveyYear.Value; } private set { _surveyYear.Value = value; } }
        private LabeledParameter<int> _surveyYear = new LabeledParameter<int>("odpt:surveyYear", 0);

        /// <summary>
        /// odpt:passengerJourneys 	xsd:integer 	駅の1日あたりの平均乗降人員数 	◯
        /// </summary>
        public int PassengerJourneys { get { return _passengerJourneys.Value; } private set { _passengerJourneys.Value = value; } }
        private LabeledParameter<int> _passengerJourneys = new LabeledParameter<int>("odpt:passengerJourneys", 0);

    }
}
