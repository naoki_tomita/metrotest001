﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Reflection;

namespace MetroTest001
{

    public interface IJsonLoadable
    {
        void Load(JToken source);
        //string GetInformation();
        void Disp(Action<string> act);
    }

    public class LabeledParameter<T> : IJsonLoadable
    {
        public T Value { get; set; }
        public string Key { get; set; }
        public T DefaultValue { get; set; }


        public LabeledParameter(string key, T defaultValue)
            : this(key)
        {
            //this.Key = key;
            this.DefaultValue = defaultValue;
        }
        public LabeledParameter(string key)
        {
            this.Key = key;
        }
        public void Load(JToken source)
        {
            //Value = source.Value<T>(Key) ?? DefaultValue;
            Value = source.Value<T>(Key);
            if (Value == null)
            {
                Value = DefaultValue;
            }
        }
        public void Disp(Action<string> act)
        {
            act(Key + " = " + ((Value == null) ? "null" : Value.ToString()));
        }
    }

    /// <summary>
    /// データ型のリスト
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LabeledList<T> : IJsonLoadable where T : IJsonLoadable, new()
    {
        public List<T> Value { get; private set; }
        public string Key { get; set; }


        public LabeledList(string key)
        {
            Value = new List<T>();
            this.Key = key;
        }

        public void Load(JToken source)
        {
            this.Value.Clear();

            //var array = source.Value<JToken>(Key);
            var array = source[Key];


            if (array == null)
            {
                return;
            }

            foreach (var elm in array)
            {
                var inst = new T();
                inst.Load(elm);
                this.Value.Add(inst);
            }
        }
        public void Disp(Action<string> act)
        {
            act(Key + " = List<" + typeof(T).ToString() + ">[" + Value.Count.ToString() + "]");


            //return;
            foreach (var elm in this.Value)
            {
                elm.Disp(act);
                return;
                //act(Key + " = " + Value.ToString());
            }
        }
    }

    /// <summary>
    /// 値型のリスト
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LabeledValueList<T> : IJsonLoadable //where T : struct
    {
        public List<T> Value { get; private set; }
        public string Key { get; set; }


        public LabeledValueList(string key)
        {
            Value = new List<T>();
            this.Key = key;
        }

        public void Load(JToken source)
        {
            this.Value.Clear();

            IEnumerable<T> array;
            try
            {
                //array = source.Value<JArray>(Key).Values<T>();
                var collection = source[Key];
                if (collection == null)
                {
                    return;
                }

                if (collection.HasValues)
                {
                    array = source[Key].Values<T>();
                }
                else
                {

                    array = new List<T> { source.Value<T>(Key) };
                }
            }
            catch (NullReferenceException)
            {
                return;
                //array = new List<T>();
            }

            this.Value = new List<T>(array);
        }
        public void Disp(Action<string> act)
        {
            act(Key + " = List<" + typeof(T).ToString() + ">[" + Value.Count.ToString() + "]");

            //return;
            foreach (var elm in this.Value)
            {
                //elm.Disp(act);
                //return;
                act(Key + " = " + elm.ToString());
            }
        }
    }

    /// <summary>
    /// IJsonLoadableを実装したフィールドを持つクラス
    /// </summary>
    public class JsonObject : IJsonLoadable
    {
        // JSONデータから値を読み込むフィールドのリスト
        private List<IJsonLoadable> _container = null;
        private List<IJsonLoadable> Container
        {
            get
            {
                if (_container == null)
                {
                    _container = InitContainer();
                }
                return _container;
            }
        }

        /// <summary>
        /// IJsonLoadableを実装しているフィールドを列挙してcontainerに格納
        /// </summary>
        /// <returns></returns>
        private List<IJsonLoadable> InitContainer()
        {
            return new List<IJsonLoadable>(
                this.GetType()
                .GetFields(
                BindingFlags.Public | BindingFlags.NonPublic
                | BindingFlags.Instance | BindingFlags.Static)
                .Select(elm => elm.GetValue(this) as IJsonLoadable)
                .Where(v => v != null));
        }

        public void Load(JToken source)
        {
            foreach (var param in Container)
            {
                param.Load(source);
            }
        }
        public void Disp(Action<string> act)
        {
            foreach (var param in Container)
            {
                param.Disp(act);
            }
        }
    }
}
